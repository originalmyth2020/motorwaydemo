//
//  MainViewController.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var wordCountLabel: UILabel!
    @IBOutlet weak var mainTextView: UITextView!
    
    lazy var viewModel: MainViewModel = {
        return MainViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        modifyKeyboard()
        setupListeners()
        setupDelegates()
        viewModel.initaliseDisplay()
    }
    
    private func setupDelegates() {
        mainTextView.delegate = self
    }

    private func setupListeners() {
        viewModel.reloadLabelCountListener = updateMainCountLabel
        viewModel.reloadMainTextListener = updateMainText
    }
    
    private func modifyKeyboard() {
        let toolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: view.frame.size.width, height: Constants.keyboardDoneTextHeight))
         let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: Constants.keyboardDoneText, style: .done, target: self, action: #selector(dismissKeyboard))
         toolbar.setItems([flexSpace, doneBtn], animated: false)
         toolbar.sizeToFit()
        mainTextView.inputAccessoryView = toolbar
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func updateMainText(value: String) {
        mainTextView.text = value
    }
    
    func updateMainCountLabel(value: String) {
        wordCountLabel.text = value
    }

}

extension MainViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        viewModel.updateMainText(text: textView.text)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.persistChanges()
    }

}
