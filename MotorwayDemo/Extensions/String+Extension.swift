//
//  String+Extension.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import Foundation

extension String {
    var wordCount: Int {
        var count = 0
        let range = startIndex..<endIndex
        enumerateSubstrings(in: range, options: [.byWords, .substringNotRequired, .localized], { _, _, _, _ -> () in
            count += 1
        })
        return count
    }
}
