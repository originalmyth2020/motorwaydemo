//
//  UserStorage.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import Foundation

protocol StorageProtocol {
    func store<T>(key: String, value: T)
    func retrieve<T>(key: String, value: T.Type) -> T?
}

class UserStorage: StorageProtocol {
    
    private let userDefaults = UserDefaults.standard
    
    static let sharedInstance: UserStorage = {
        let instance = UserStorage()
        return instance
    }()
    
    private init() {}
    
    func store<T>(key: String, value: T) {
        userDefaults.set(value, forKey: key)
    }
    
    func retrieve<T>(key: String, value: T.Type) -> T? {
        return userDefaults.object(forKey: key) as? T
    }
    
}
