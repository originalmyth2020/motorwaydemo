//
//  Constants.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import UIKit

struct Constants {
    static let mainText = "mainTextKey"
    static let keyboardDoneText = "Done"
    static let keyboardDoneTextHeight: CGFloat = 30.0
    static let wordCountText = "Word count is:"
}
