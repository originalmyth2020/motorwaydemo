//
//  DebugPrint.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import Foundation

func debugPrint(_ objects: Any...) {
    #if DEBUG
    for item in objects {
        print(item)
    }
    #endif
}
