//
//  MainViewModel.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import Foundation

typealias ReloadViewListener = (String) -> Void

class MainViewModel {
    
    var reloadLabelCountListener: ReloadViewListener?
    var reloadMainTextListener: ReloadViewListener?
    
    var mainCoordinator: MainCoordinatorProtocol
    
    init(mainCoordinator: MainCoordinatorProtocol = MainCoordinator.sharedInstance) {
        self.mainCoordinator = mainCoordinator
    }
    
    private var mainLabelCount: String = "" {
        didSet {
            reloadLabelCountListener?(mainLabelCount)
        }
    }
    
    private var mainDataModel: DataModel? = nil {
        didSet {
            let text = mainDataModel?.displayData ?? ""
            reloadMainTextListener?(text)
            updateMainLabel(text: text)
        }
    }

    func initaliseDisplay() {
        mainCoordinator.fetchData(completion: { [weak self] result in
            switch result {
            case .success(let data):
                guard let data = data else {
                    debugPrint("No valid data")
                    return
                }
                self?.mainDataModel = data
            case .failure(let error):
                debugPrint("Error is \(error)")
            }
        })
    }
    
    func updateMainText(text: String) {
        guard mainDataModel != nil else {
            debugPrint("mainDataModel is nil")
            return
        }
        mainDataModel?.displayData = text
    }
    
    func updateMainLabel(text: String) {
        mainLabelCount = "\(Constants.wordCountText) \(text.wordCount)"
    }
    
    func persistChanges() {
        mainCoordinator.saveData(dataModel: mainDataModel)
    }
    
}
