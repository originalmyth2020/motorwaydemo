//
//  AppError.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import Foundation

enum AppError: Error {
    case defaultError(String)
}
