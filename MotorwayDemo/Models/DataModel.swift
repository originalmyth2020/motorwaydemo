//
//  DataModel.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import Foundation


struct DataModel {
    
    var displayData =
        """
    It killed humans, therefore it was a weapon. But radiation killed humans, and a medical X-ray machine wasn’t intended as a weapon. Holden was starting to feel like they were all monkeys playing with a microwave. Push a button, a light comes on inside, so it’s a light. Push a different button and stick your hand inside, it burns you, so it’s a weapon. Learn to open and close the door, it’s a place to hide things. Never grasping what it actually did, and maybe not even having the framework necessary to figure it out. No monkey ever reheated a frozen burrito.
    """
    
}
