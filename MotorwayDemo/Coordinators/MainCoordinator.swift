//
//  MainCoordinator.swift
//  MotorwayDemo
//
//  Created by Duncan on 07/06/2021.
//

import Foundation

protocol MainCoordinatorProtocol {
    func fetchData(completion: @escaping(Result<DataModel?, AppError>) -> Void)
    func saveData(dataModel: DataModel?)
}

class MainCoordinator: MainCoordinatorProtocol {
    
    var persistenceStore: StorageProtocol = UserStorage.sharedInstance
    
    static let sharedInstance: MainCoordinator = {
        let instance = MainCoordinator()
        return instance
    }()
    
    func fetchData(completion: @escaping(Result<DataModel?, AppError>) -> Void) {
        guard let mainUserDisplayString = persistenceStore.retrieve(key: Constants.mainText, value: String.self) else {
            completion(.success(DataModel()))
            return
        }
        completion(.success(DataModel(displayData: mainUserDisplayString)))
    }
    
    func saveData(dataModel: DataModel?) {
        DispatchQueue.global().async { [weak self] in
            self?.persistenceStore.store(key: Constants.mainText, value: dataModel?.displayData)
        }
    }
    
}
