//
//  MainViewModelTests.swift
//  MotorwayDemoTests
//
//  Created by Duncan on 07/06/2021.
//

import XCTest
@testable import MotorwayDemo

class MainViewModelTests: XCTestCase {

    
    func testViewModelLoadData() {
        let wordCountTotalString = "103"
        let expect = expectation(description: "Load data")
        let homeViewModelTest = MainViewModelTestWrapper()
        homeViewModelTest.initaliseDisplay()
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            XCTAssertNotNil(homeViewModelTest.mainTextString, "mainTextString should return valid object")
            XCTAssertNotNil(homeViewModelTest.wordCountString, "wordCountString should return valid object")
            let doesContain = homeViewModelTest.wordCountString?.contains(wordCountTotalString) ?? false
            XCTAssertTrue(doesContain, "Should contain word \(wordCountTotalString) count")
            expect.fulfill()
        }
        wait(for: [expect], timeout: 2)
    }
    
    func testViewModelLoadDataWithStore() {
        let wordCountTotalString = "2"
        let newText = "Monte Carlo"
        let expect = expectation(description: "Save then Load data")
        let homeViewModelTest = MainViewModelTestWrapper()
        homeViewModelTest.viewModel.mainCoordinator.saveData(dataModel: DataModel(displayData: newText))

        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            homeViewModelTest.initaliseDisplay()
            XCTAssertNotNil(homeViewModelTest.mainTextString, "mainTextString should return valid object")
            XCTAssertNotNil(homeViewModelTest.wordCountString, "wordCountString should return valid object")
            let doesContain = homeViewModelTest.wordCountString?.contains(wordCountTotalString) ?? false
            XCTAssertTrue(doesContain, "Should contain word \(wordCountTotalString) count")
            expect.fulfill()
        }
        wait(for: [expect], timeout: 2)
    }

}
