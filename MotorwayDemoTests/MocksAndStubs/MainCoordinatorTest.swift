//
//  MainCoordinatorTest.swift
//  MotorwayDemoTests
//
//  Created by Duncan on 07/06/2021.
//

import Foundation
@testable import MotorwayDemo

class MainCoordinatorTest {
    static let sharedInstanceTest: MainCoordinator = {
        let sharedInstanceTest = MainCoordinator.sharedInstance
        sharedInstanceTest.persistenceStore = TestPersistenceStore()
        return sharedInstanceTest
    }()
}

class TestPersistenceStore: StorageProtocol {
    
    var localCache = [String: Any]()
    
    func store<T>(key: String, value: T) {
        localCache[key] = value
    }
    
    func retrieve<T>(key: String, value: T.Type) -> T? {
        return localCache[key] as? T
    }
}

class MainViewModelTestWrapper {
    
    var mainTextString: String?
    var wordCountString: String?
    
    lazy var viewModel: MainViewModel = {
        return MainViewModel(mainCoordinator: MainCoordinatorTest.sharedInstanceTest)
    }()
    
    init() {
        setupListeners()
    }

    private func setupListeners() {
        viewModel.reloadLabelCountListener = updateMainCountLabel
        viewModel.reloadMainTextListener = updateMainText
    }
    
    func initaliseDisplay() {
        viewModel.initaliseDisplay()
    }

    func updateMainText(value: String) {
        mainTextString = value
    }

    func updateMainCountLabel(value: String) {
        wordCountString = value
    }
}
