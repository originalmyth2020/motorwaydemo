Built using Xcode 12.1 (Runs in Xcode 12.5) 
Compatible with iOS 13 - 14

No third libraries used. 
No special requirements needed to run or to run tests.

Testing in the simulator requires the virtual keyboard for any changes to the text to be persisted. 
(Handy reminder: Command+K to toggle the keyboard)

Assumptions:
A TextView rather than a TextField was used. 
This was because the requirements specified a paragraph of text.

Didn't add any using facing Alerts on point of failure only prints to the console in this case - could have added.

Side Point:
Text examples are Expanse quotes
